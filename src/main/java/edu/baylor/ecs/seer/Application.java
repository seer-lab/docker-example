package edu.baylor.ecs.seer;

import edu.baylor.ecs.seer.lweaver.service.ApplicationEvaluatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

    @Autowired
    private ApplicationEvaluatorService evaluatorService;

    @RequestMapping("/")
    public String home() {

        String applicationStructureInJson = evaluatorService.deriveStructure();

        return applicationStructureInJson;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
